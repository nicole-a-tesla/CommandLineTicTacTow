class BoardPrintFormatter {
  let parser: BoardParser
  let columnKeys = "   a   b   c\n"
  
  init(parser: BoardParser) {
    self.parser = parser
  }
  
  func formatBoard(board: Board) -> String {
    var boardSoFar = columnKeys
    
    for (index, row) in board.getSpaces().enumerate() {
      boardSoFar += formatRow(index, row: row)
    }
    return boardSoFar
  }
  
  func formatRow(index: Int, row: [Space]) -> String {
    var rowSoFar = getRowKey(index)
    
    for space in row {
      rowSoFar += "| \(formatSpace(space)) "
    }
    
    return rowSoFar + "|\n"
  }
  
  func formatSpace(space: Space) -> String {
    return space.isEmpty() ? "_" : space.getContents()
  }
  
  func getRowKey(index: Int) -> String {
    return intToString(index + 1)
  }
  
  func intToString(int: Int) -> String {
    return String(int)
  }
}