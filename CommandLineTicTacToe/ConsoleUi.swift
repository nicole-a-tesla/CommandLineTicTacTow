import Darwin

class ConsoleUi: Ui , NextMoveDelegate {
  let printer: Printer
  let printFormatter: BoardPrintFormatter
  let receiver: Receiver
  let validator: UserInputValidator
  let positionCoordsMap: PositionCoordsMap
  let userMessage: UserMessage
  let maxTries = 10
  
  
  init(printer: Printer,
       printFormatter: BoardPrintFormatter,
       receiver: Receiver,
       validator: UserInputValidator,
       positionCoordsMap: PositionCoordsMap,
       userMessage: UserMessage) {
    
    self.printer = printer
    self.printFormatter = printFormatter
    self.receiver = receiver
    self.validator = validator
    self.positionCoordsMap = positionCoordsMap
    self.userMessage = userMessage
  }
  
  func requestNextMove(gameState: GameState, marker: String) -> Coords{
    return requestPositionUntilValid(gameState.getBoard())
  }
  
  override func renderBoard(board: Board) {
    let formattedBoard = printFormatter.formatBoard(board)
    printIt(formattedBoard)
  }
  
  override func requestPositionUntilValid(board: Board) -> Coords {
    var input: String?
    var tryCount = 0
    
    while !validator.isValid(input, board: board) {
      respondToFailures(tryCount)
      input = requestPosition()
      tryCount += 1
    }
    
    return translateToCoords(input!)
  }
  
  func requestPosition() -> String? {
    printIt(userMessage.promptMove())
    return receiver.getInput()
  }
  
  func respondToFailures(tryCount: Int) {
    scoldIfFailure(tryCount)
    exitIfExcessiveFailure(tryCount)
  }
  
  func scoldIfFailure(tryCount: Int) {
    if tryCount >= 1 {
      printIt(userMessage.choiceInvalid())
    }
  }
  
  func exitIfExcessiveFailure(tryCount: Int) {
    if tryCount >= maxTries {
      printIt(userMessage.fail())
      exit(0)
    }
  }
  
  func translateToCoords(userInput: String) -> Coords {
    return positionCoordsMap.translate(userInput)
  }
  
  override func handleGameEnd(gameState: GameState) {
    outputBoard(gameState.getBoard())
    printIt(userMessage.gameOver())
    if gameState.hasWinner() {
      printIt(userMessage.announceWinner(gameState.getInactiveMarker()))
    } else {
      printIt(userMessage.announceTie())
    }
    
  }
  
  override func announceNewTurn(gameState: GameState) {
    announcePlayerTurn(gameState.getActiveMarker())
    outputBoard(gameState.getBoard())
  }
  
  func announcePlayerTurn(marker: String) {
    printIt(userMessage.yourTurn(marker))
  }
  
  func outputBoard(board: Board) {
    printIt(printFormatter.formatBoard(board))
  }
  
  func printIt(info: String) {
    printer.printIt(info)
  }
}
