class Space : Equatable {
  private var innerContents: String
  
  init(contents: String="Empty") {
    innerContents = contents
  }
  
  func getContents() -> String {
    return innerContents
  }
  
  func isEmpty() -> Bool {
    return innerContents == "Empty"
  }
}

func ==(spaceA: Space, spaceB: Space) -> Bool {
  return spaceA.getContents() == spaceB.getContents()
}