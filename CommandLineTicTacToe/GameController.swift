class GameController {
  var gameState: GameState
  let judge: Judge
  let ui: Ui
  
  init(gameState: GameState, judge: Judge, ui: Ui) {
    self.gameState = gameState
    self.judge = judge
    self.ui = ui
  }
  
  func getGameState() -> GameState {
    return gameState
  }
  
  func start() {
    while !judge.isGameOver(gameState) {
      takeTurn()
    }
    
    updateGameState()
    ui.handleGameEnd(gameState)
  }
  
  func updateGameState() {
    gameState.setGameOverToTrue()
    if judge.isGameWon(gameState) {
      gameState.setWinner(judge.getWinner(gameState))
    }
  }
  
  func takeTurn() {
    ui.announceNewTurn(gameState)
    let activePlayer = gameState.getActivePlayer()
    let nextMove = activePlayer.getMove(gameState)
    self.gameState = gameState.getNextState(nextMove)
  }
}