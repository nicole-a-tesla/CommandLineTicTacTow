struct Coords : Equatable {
  let x, y: Int
  
  init(x:Int, y:Int) {
    self.x = x
    self.y = y
  }
  
}

func ==(coordsA: Coords, coordsB: Coords) -> Bool {
  return coordsA.x == coordsB.x && coordsA.y == coordsB.y
}
