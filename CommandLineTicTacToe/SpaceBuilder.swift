class SpaceBuilder {
  func build() -> [[Space]] {
    return [[Space(), Space(), Space()],
            [Space(), Space(), Space()],
            [Space(), Space(), Space()]]
  }
}