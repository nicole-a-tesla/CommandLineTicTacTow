class GameState {
  private var gameOver: Bool
  private var winner: Player
  private var board: Board
  private var activePlayer: Player
  private var inactivePlayer: Player
  
  init(board: Board=Board(spaceBuilder: SpaceBuilder()), activePlayer: Player, inactivePlayer: Player) {
    self.gameOver = false
    self.winner = NullPlayer()
    self.board = board
    self.activePlayer = activePlayer
    self.inactivePlayer = inactivePlayer
  }
  
  func getActivePlayer() -> Player {
    return self.activePlayer
  }
  
  func getActiveMarker() -> String {
    return self.activePlayer.getMarker()
  }
  
  func getInactivePlayer() -> Player {
    return self.inactivePlayer
  }
  
  func getInactiveMarker() -> String {
    return self.inactivePlayer.getMarker()
  }
  
  func isOver() -> Bool {
    return self.gameOver
  }
  
  func setGameOverToTrue() {
    self.gameOver = true
  }
  
  func hasWinner() -> Bool {
    return winner.getMarker() != "Null"
  }
  
  func getWinner() -> Player {
    return self.winner
  }
  
  func getWinningMarker() -> String {
    return self.winner.getMarker()
  }
  
  func setWinner(marker: String) {
    self.winner = playerForMarker(marker)
    self.setGameOverToTrue()
  }
  
  func playerForMarker(marker: String) -> Player {
    if activePlayer.getMarker() == marker {
      return activePlayer
    } else if inactivePlayer.getMarker() == marker {
      return inactivePlayer
    } else {
      return NullPlayer()
    }
  }
  
  func getBoard() -> Board {
    return self.board
  }
  
  func getSpace(coords: Coords) -> Space {
    return self.board.getSpace(coords)
  }
  
  func getNextState(spaceToModify: Coords) -> GameState {
    let newBoard = self.board.clone()
    newBoard.setSpaceContents(spaceToModify, newContents: getActiveMarker())
    return GameState(board: newBoard, activePlayer: getInactivePlayer(), inactivePlayer: getActivePlayer())
  }
}