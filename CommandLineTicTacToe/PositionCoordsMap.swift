struct PositionCoordsMap {
  let defaultCoords = Coords(x: 0, y: 0)
  
  let innerMap = ["1a": Coords(x: 0, y: 0), "1b": Coords(x: 0, y: 1), "1c": Coords(x: 0, y: 2),
                  "2a": Coords(x: 1, y: 0), "2b": Coords(x: 1, y: 1), "2c": Coords(x: 1, y: 2),
                  "3a": Coords(x: 2, y: 0), "3b": Coords(x: 2, y: 1), "3c": Coords(x: 2, y: 2)]
  
  func translate(position: String) -> Coords {
    return innerMap[position] ?? defaultCoords
  }
  
  func keys() -> LazyMapCollection<Dictionary<String, Coords>, String> {
    return innerMap.keys
  }
}