class Minimaxer:  NextMoveDelegate {
  private let marker: String
  private let opponentMarker: String
  private let judge: Judge
  private let boardParser: BoardParser
  private var choice: Coords!
  
  init(marker: String="O", opponentMarker: String="X", judge: Judge=Judge(), boardParser: BoardParser=BoardParser()) {
    self.marker = marker
    self.opponentMarker = opponentMarker
    self.judge = judge
    self.boardParser = boardParser
  }
  
  func requestNextMove(gameState: GameState, marker: String) -> Coords{
    return getMove(gameState)
  }
  
  func getMarker() -> String {
    return marker
  }
  
  func getOpponentMarker() -> String {
    return opponentMarker
  }
  
  func getMove(gameState: GameState) -> Coords {
    calculateMove(gameState)
    return getChoice()
  }
  
  func getChoice() -> Coords {
    return choice
  }
  
  internal func setChoice(coords: Coords) {
    choice = coords
  }
  
  func score(gameState: GameState, depth: Int=0) -> Int {
    switch judge.getWinner(gameState) {
    case marker:
      return 10 - depth
    case opponentMarker:
      return depth - 10
    default:
      return 0
    }
  }
  
  func calculateMove(gameState: GameState, depth: Int=0) -> Int {
    if judge.isGameOver(gameState) {
      return score(gameState, depth: depth)
    }
    
    let newDepth = depth + 1
    
    var localCoords = [Coords]()
    var localScores = [Int]()
    
    for availableCoord in boardParser.getAvailableSpaces(gameState.getBoard()) {
      let nextGameState = gameState.getNextState(availableCoord)
      localCoords.append(availableCoord)
      localScores.append(calculateMove(nextGameState, depth: newDepth))
    }
    
    let (chosenScore, chosenCoords) = getBestScoreAndCorrespondingCoords(gameState, scores: localScores, coords: localCoords)
    setChoice(chosenCoords)
    return chosenScore
    
  }
  
  func getBestScoreAndCorrespondingCoords(gameState: GameState, scores: [Int], coords: [Coords]) -> (Int, Coords) {
    let chosenScore = chooseScore(gameState, scores: scores)
    let indexOfChoice = scores.indexOf(chosenScore)!
    let chosenCoords = coords[indexOfChoice]
    return (chosenScore, chosenCoords)
  }
  
  func chooseScore(gameState: GameState, scores: [Int]) -> Int {
    return gameState.getActiveMarker() == marker ? scores.maxElement()! : scores.minElement()!
  }
}