protocol NextMoveDelegate: class {
  func requestNextMove(gameState: GameState, marker: String) -> Coords
}

class Player {
  
  private var marker:String
  weak var delegate:NextMoveDelegate?
  
  init(marker:String, nextMoveDelegate: NextMoveDelegate?) {
    self.marker = marker
    self.delegate = nextMoveDelegate
  }
  
  func getMarker() -> String {
    return marker
  }
  
  func getMove(gameState: GameState) -> Coords {
    return (delegate?.requestNextMove(gameState, marker:marker))!
  }
  
}