class ConsoleReceiver: Receiver {
  override func getInput() -> String? {
    return readLine()
  }
}