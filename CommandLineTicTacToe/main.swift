let boardParser = BoardParser()

let consolePrinter = ConsolePrinter()
let boardFormatter = BoardPrintFormatter(parser: boardParser)
let reciever = ConsoleReceiver()
let validator = UserInputValidator()
let positionCoordsMap = PositionCoordsMap()
let userMessage = UserMessage()

let consoleUi = ConsoleUi(printer: consolePrinter, printFormatter: boardFormatter, receiver: reciever, validator: validator, positionCoordsMap: positionCoordsMap, userMessage: userMessage)
let miniMaxer = Minimaxer()

let computerPlayer = Player(marker: "O", nextMoveDelegate: miniMaxer)
let humanPlayer = Player( marker: "X", nextMoveDelegate: consoleUi)

let spaceBuilder = SpaceBuilder()

let gameState = GameState(board: Board(spaceBuilder: spaceBuilder), activePlayer: humanPlayer, inactivePlayer: computerPlayer)
let controller = GameController(gameState: gameState, judge: Judge(), ui: consoleUi)

controller.start()