class BoardParser {
  
  func getRow(rowId:Int, board:Board) -> [Space] {
    return board.getSpaces()[rowId]
  }
  
  func getColumn(columnId:Int, board:Board) -> [Space] {
    return board.getSpaces().map{ $0[columnId] }
  }
  
  func getLtrDiagonal(board:Board) -> [Space] {
    return [0,1,2].map{ board.getSpaces()[$0][$0] }
  }
  
  func getRtlDiagonal(board: Board) -> [Space] {
    return [0,1,2].map{ board.getSpaces()[2-$0][$0] }
  }
  
  func flatten(board: Board) -> [Space] {
    return board.getSpaces().flatMap { $0 }
  }
  
  func getContentsOnly(subBoard: [Space]) -> [String] {
    return subBoard.map{ $0.getContents() }
  }
  
  func uniqueContents(subBoard: [Space]) -> [String] {
    return Array(Set(getContentsOnly(subBoard)))
  }
  
  func getAllSubBoards(board: Board) -> [[Space]] {
    return getRowsAndColumns(board) + getDiagonals(board)
  }
  
  internal func getRowsAndColumns(board: Board) -> [[Space]] {
    return board.getSpaces() + getAllColumns(board)
  }
  
  func getAllColumns(board: Board) -> [[Space]] {
    return [0,1,2].map { getColumn($0, board: board) }
  }
  
  func getDiagonals(board: Board) -> [[Space]] {
    return [getLtrDiagonal(board), getRtlDiagonal(board)]
  }
  
  func getFirstElement(subBoard: [Space]) -> Space {
    return subBoard[0]
  }
  
  func getAvailableSpaces(board: Board) -> [Coords] {
    var availableSpaces = [Coords]()
    
    for (xVal, row) in board.getSpaces().enumerate() {
      for (yVal, space) in row.enumerate() {
        if space.isEmpty() {
          availableSpaces.append(Coords(x:xVal, y:yVal))
        }
      }
    }
    return availableSpaces
  }
  
}