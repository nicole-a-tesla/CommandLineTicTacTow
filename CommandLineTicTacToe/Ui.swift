class Ui {
  
  func renderBoard(board: Board) {}
  
  func requestPositionUntilValid(board: Board) -> Coords {
    return Coords(x: 0, y: 0)
  }
  
  func handleGameEnd(gameState: GameState) {}
  
  func announceNewTurn(gameState: GameState) {}
}
