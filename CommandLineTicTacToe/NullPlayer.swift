class NullPlayer: Player {
  
  override init(marker:String="Null", nextMoveDelegate: NextMoveDelegate?=nil) {
    super.init(marker: "Null", nextMoveDelegate: nextMoveDelegate)
  }
  
}