class Judge {
  let boardParser: BoardParser
  
  init(boardParser: BoardParser=BoardParser()) {
    self.boardParser = boardParser
  }
  
  func isGameOver(gameState: GameState) -> Bool {
    return isTie(gameState) || isGameWon(gameState)
  }
  
  func isGameWon(gameState: GameState) -> Bool {
    return getPossibleWinningSubBoards(gameState.getBoard()).count == 1
  }
  
  func getWinner(gameState: GameState) -> String {
    let possibleWinningSubBoard = getPossibleWinningSubBoards(gameState.getBoard())
    return isGameWon(gameState) ? getMarkerFromWinningSubBoard(possibleWinningSubBoard) : "Null"
  }
  
  func isTie(gameState: GameState) -> Bool {
    let flatBoard = boardParser.flatten(gameState.getBoard())
    let remainingemptySpaces = flatBoard.filter { $0.isEmpty() }
    return remainingemptySpaces.isEmpty && !isGameWon(gameState)
  }
  
  internal func getPossibleWinningSubBoards(board: Board) -> [[Space]] {
    let subBoards = boardParser.getAllSubBoards(board)
    return subBoards.filter{ checkForSubBoardWin($0) }
  }
  
  internal func checkForSubBoardWin(subBoard: [Space]) -> Bool {
    let firstElement = boardParser.getFirstElement(subBoard)
    return !firstElement.isEmpty() && allElementsIdentical(subBoard)
  }
  
  internal func allElementsIdentical(subBoard: [Space]) -> Bool {
    return boardParser.uniqueContents(subBoard).count == 1
  }
  
  internal func getMarkerFromWinningSubBoard(subBoardArray: [[Space]]) -> String {
    let subBoard = subBoardArray[0]
    return boardParser.getFirstElement(subBoard).getContents()
  }
}

