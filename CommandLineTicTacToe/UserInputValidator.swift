class UserInputValidator {
  let positionsMap: PositionCoordsMap
  
  init(positionsMap: PositionCoordsMap=PositionCoordsMap()) {
    self.positionsMap = positionsMap
  }
  
  func isValid(input: String?, board: Board) -> Bool {
    return inputExists(input) && isOnBoard(input!) && isAvailable(input!, board: board)
  }
  
  func inputExists(input: String?) -> Bool {
    return input != nil
  }
  
  func isOnBoard(input: String) -> Bool {
    return positionsMap.keys().contains(input)
  }
  
  func isAvailable(input: String, board: Board) -> Bool {
    let coords = positionsMap.translate(input)
    return board.isSpaceEmpty(coords)
  }
  
}