class UserMessage {
  func yourTurn(marker: String) -> String {
    return "It's your turn, \(marker)!"
  }
  
  func fail() -> String {
    return "Seems like you're having a hard time. Maybe try again later."
  }
  
  func choiceInvalid() -> String {
    return "That's not a valid position. Pick a valid position"
  }
  
  func gameOver() -> String {
    return "Game Over"
  }
  
  func announceWinner(marker: String) -> String {
    return "\(marker) Wins"
  }
  
  func announceTie() -> String {
    return "It's a tie"
  }
  
  func promptMove() -> String {
    return "Enter a number+letter combination i.e: 2b"
  }
}