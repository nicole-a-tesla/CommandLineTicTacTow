class Board {
  private var spaces: [[Space]]
  private let spaceBuilder: SpaceBuilder
  
  init(spaceBuilder: SpaceBuilder) {
    self.spaceBuilder = spaceBuilder
    spaces = spaceBuilder.build()
  }
  
  func getSpaces() -> [[Space]] {
    return spaces
  }
  
  func isSpaceEmpty(coords: Coords) -> Bool {
    return spaces[coords.x][coords.y].isEmpty()
  }
  
  func getSpaceContents(coords: Coords) -> String {
    return spaces[coords.x][coords.y].getContents()
  }
  
  func setSpaceContents(coords: Coords, newContents: String) {
    spaces[coords.x][coords.y] = Space(contents: newContents)
  }
  
  func getSpace(coords: Coords) -> Space {
    return spaces[coords.x][coords.y]
  }
  
  func clone() -> Board {
    let clone = Board(spaceBuilder: spaceBuilder)
    
    for (xIndex, row) in spaces.enumerate() {
      for (yIndex, space) in row.enumerate() {
        clone.setSpaceContents(Coords(x: xIndex, y: yIndex), newContents: space.getContents())
      }
    }
    return clone
  }
}