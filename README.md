## Swift TicTacToe

A terminal-based tic tac toe game.

## Setup
The default project settings are compatible with Swift 2.2 and Xcode 7.3. If you'd like to run it in older ecosystems, change the version of Quick specified in the Podfile to one that is compadible with your setup. Quick release info can be found [here](https://github.com/Quick/Quick/releases)

### To play the game
<br/> If you have not already, [install Xcode Command Line Tools](https://developer.apple.com/library/ios/technotes/tn2339/_index.html) with `$ xcode-select --install`
<br/>Clone the repo
<br/>In your terminal, run `make` in the project root
<br/>Start the game by running `./tictactoe`

### To run the tests
<br/>If you have not already, [download and install Xcode](https://itunes.apple.com/us/app/xcode/id497799835?mt=12)
<br/>If you have not already, install [Cocoapods](https://cocoapods.org/) with `$ sudo gem install cocoapods`
<br/>Clone the repo
<br/>In the root of the project, run `pod install` to install the testing dependencies
<br/>Open the project's `.xcworkspace` file in Xcode
<br/>Add the test target:
1. Click on the CommandLineTicTacToe project in the left toolbar.
<br/>
![Add Test Target 1](https://gitlab.com/nicole-a-tesla/CommandLineTicTacTow/raw/master/Images/addTests1.png "Add Test Target 1")
<br/>
<br/>
2. Hold down the play button in the upper left-hand corner to open its drop down menu. Select "Test"
<br/>
<br/>
![Add Test Target 2](https://gitlab.com/nicole-a-tesla/CommandLineTicTacTow/raw/master/Images/addTests2.png "Add Test Target 2")
<br/>
<br/>
3. Select "Edit Scheme" on the popup that appears
<br/>
<br/>
![Add Test Target 3](https://gitlab.com/nicole-a-tesla/CommandLineTicTacTow/raw/master/Images/addTests3.png "Add Test Target 3")
<br/>
<br/>
4. Select the "+" at the bottom left of the screen
<br/>
<br/>
![Add Test Target 4](https://gitlab.com/nicole-a-tesla/CommandLineTicTacTow/raw/master/Images/addTests4.png "Add Test Target 4")
<br/>
<br/>
5. Select "CommandLineTicTacToeTests and click "Add"
<br/>
<br/>
![Add Test Target 5](https://gitlab.com/nicole-a-tesla/CommandLineTicTacTow/raw/master/Images/addTests5.png "Add Test Target 5")
<br/>
<br/>
6. Close the menu. Cmd + U should now run the tests
