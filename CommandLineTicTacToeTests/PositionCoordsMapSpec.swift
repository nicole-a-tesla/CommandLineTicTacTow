import Nimble
import Quick

class PositionCoordsMapSpec: QuickSpec {
  override func spec() {
    
    describe("position to coord map") {
      let map = PositionCoordsMap()
      
      it("translates 1a") {
        expect(map.translate("1a")).to(equal(Coords(x: 0, y: 0)))
      }
      
      it("translates 1b") {
        expect(map.translate("1b")).to(equal(Coords(x: 0, y: 1)))
      }
      
      
      it("translates 1c") {
        expect(map.translate("1c")).to(equal(Coords(x: 0, y: 2)))
      }
      
      
      it("translates 2a") {
        expect(map.translate("2a")).to(equal(Coords(x: 1, y: 0)))
      }
      
      
      it("translates 2b") {
        expect(map.translate("2b")).to(equal(Coords(x: 1, y: 1)))
      }
      
      
      it("translates 2c") {
        expect(map.translate("2c")).to(equal(Coords(x: 1, y: 2)))
      }
      
      
      it("translates 3a") {
        expect(map.translate("3a")).to(equal(Coords(x: 2, y: 0)))
      }
      
      
      it("translates 3b") {
        expect(map.translate("3b")).to(equal(Coords(x: 2, y: 1)))
      }
      
      it("translates 3c") {
        expect(map.translate("3c")).to(equal(Coords(x: 2, y: 2)))
      }
      
      
      it("handles nonsense gracefully") {
        expect(map.translate("hi there")).to(equal(Coords(x: 0, y: 0)))
      }
      
      
    }
  }
}