import Nimble
import Quick

class BoardPrintFormatterSpec: QuickSpec {
  override func spec() {
    
    describe("a print formatter") {
      let emptySpace = Space()
      let expectedEmptyRow = "| _ | _ | _ |\n"
      let formatter = BoardPrintFormatter(parser: BoardParser())
      
      
      it("formats empty row") {
        let actualRow = formatter.formatRow(0, row: [emptySpace, emptySpace, emptySpace])
        expect(actualRow).to(equal("1" + expectedEmptyRow))
      }
      
      it("formats non-empty row") {
        let formatted = formatter.formatRow(0, row: [Space(contents: "X"), emptySpace, Space(contents: "O")])
        let expectedOutput = "| X | _ | O |\n"
        expect(formatted).to(equal("1" + expectedOutput))
      }
      
      it("formats whole board") {
        let expectedBoard = "\(formatter.columnKeys)1\(expectedEmptyRow)2\(expectedEmptyRow)3\(expectedEmptyRow)"
        expect(formatter.formatBoard(Board(spaceBuilder: SpaceBuilder()))).to(equal(expectedBoard))
      }
      
      it("translates ints to strings") {
        expect(formatter.intToString(0)).to(equal("0"))
      }
      
    }
    
  }
}