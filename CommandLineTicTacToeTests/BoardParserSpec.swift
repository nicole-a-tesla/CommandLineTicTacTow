import Nimble
import Quick

class BoardParserSpec: QuickSpec {
  override func spec() {
    
    describe("a board parser") {
      
      let boardParser = BoardParser()
      var board: Board!
      
      beforeEach {
        board = Board(spaceBuilder: SpaceBuilder())
      }
      
      context("row parsing") {
        it("returns first row") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:0),Coords(x:0,y:1),Coords(x:0,y:2)], newContents: "X")
          let firstRow = boardParser.getRow(0, board: board)
          
          expect(firstRow.count).to(equal(3))
          expect(allContentsEqualThis(firstRow, expectedContents: "X")).to(equal(true))
        }
        
        it("returns second row") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:1,y:0), Coords(x:1,y:1), Coords(x:1,y:2)], newContents: "X")
          let secondRow = boardParser.getRow(1, board: board)
          
          expect(allContentsEqualThis(secondRow, expectedContents: "X")).to(equal(true))
        }
        
        it("returns third row") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:2,y:0),Coords(x:2,y:1),Coords(x:2,y:2)], newContents: "X")
          let thirdRow = boardParser.getRow(2, board: board)
          
          expect(allContentsEqualThis(thirdRow, expectedContents: "X")).to(equal(true))
        }
      }
      
      context("column parsing") {
        it("returns leftmost column") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:0),Coords(x:1,y:0),Coords(x:2,y:0)], newContents: "X")
          let firstCol = boardParser.getColumn(0, board: board)
          expect(firstCol.count).to(equal(3))
          expect(allContentsEqualThis(firstCol, expectedContents: "X")).to(equal(true))
        }
        
        it("returns middle column") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:1),Coords(x:1,y:1),Coords(x:2,y:1)], newContents: "X")
          let secondCol = boardParser.getColumn(1, board: board)
          expect(allContentsEqualThis(secondCol, expectedContents: "X")).to(equal(true))
        }
        
        it("returns rightmost column") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:2),Coords(x:1,y:2),Coords(x:2,y:2)], newContents: "X")
          let thirdCol = boardParser.getColumn(2, board: board)
          expect(allContentsEqualThis(thirdCol, expectedContents: "X")).to(equal(true))
        }
      }
      
      context("diagonal parsing") {
        it("returns left-to-right diagonal") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:0),Coords(x:1,y:1),Coords(x:2,y:2)], newContents: "X")
          let ltrDiagonal = boardParser.getLtrDiagonal(board)
          expect(ltrDiagonal.count).to(equal(3))
          expect(allContentsEqualThis(ltrDiagonal, expectedContents: "X")).to(equal(true))
        }
        
        it("returns right-to-left diagonal") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:2),Coords(x:1,y:1),Coords(x:2,y:0)], newContents: "X")
          let rtlDiagonal = boardParser.getRtlDiagonal(board)
          expect(rtlDiagonal.count).to(equal(3))
          expect(allContentsEqualThis(rtlDiagonal, expectedContents: "X")).to(equal(true))
        }
      }
      
      context("additional transformations") {
        it(".flatten returns flattened board") {
          let flattenedBoard: [Space] = boardParser.flatten(board)
          expect(flattenedBoard.count).to(equal(9))
        }
        
        it(".getContentOnly returns multidimentional board array as contents instead of spaces") {
          let contentsOnly: [String] = boardParser.getContentsOnly(board.getSpaces()[0])
          expect(contentsOnly.count).to(equal(3))
          expect(contentsOnly[1]).to(equal("Empty"))
        }
        
        it(".uniqueContents returns unique elements of duplicative array") {
          let allTheSame = [Space(contents: "x"), Space(contents: "x"), Space(contents: "x")]
          expect(boardParser.uniqueContents(allTheSame)).to(equal(["x"]))
        }
        
        it(".uniqueContents returns unique elements of varried array") {
          let varried = [Space(contents: "x"), Space(contents: "x"), Space(contents: "o")]
          expect(boardParser.uniqueContents(varried).count).to(equal(2))
          expect(boardParser.uniqueContents(varried).contains("x")).to(equal(true))
          expect(boardParser.uniqueContents(varried).contains("o")).to(equal(true))
        }
        
        it(".getAllSubBoards returns array of all rows, columns, and diagonals") {
          let allSubBoards: [[Space]] = boardParser.getAllSubBoards(board)
          expect(allSubBoards.count).to(equal(8))
        }
        
        it(".getAvailableSpaces returns array of available space positions") {
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:0,y:0), Coords(x:0,y:1), Coords(x:0,y:2)], newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: [Coords(x:1,y:0), Coords(x:1,y:1), Coords(x:1,y:2)], newContents: "O")
          let availableSpaces: [Coords] = boardParser.getAvailableSpaces(board)
          expect(availableSpaces.count).to(equal(3))
          
        }
        
        it("can return the first element from a subBoard collection") {
          let firstElement = Space(contents: "X")
          let subBoard = [firstElement, Space(), Space()]
          expect(boardParser.getFirstElement(subBoard)).to(equal(firstElement))
        }
      }
    }
    
  }
}