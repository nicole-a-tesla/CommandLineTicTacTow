import Nimble
import Quick

class MinimaxSpec: QuickSpec {
  override func spec() {
    
    describe("minimaxer") {
      let judge = Judge()
      let minimaxer = Minimaxer(judge: judge)
      var board: Board!
      let playerX = Player(marker: "X", nextMoveDelegate: nil)
      let playerO = Player(marker: "O", nextMoveDelegate: nil)
      
      beforeEach {
        board = Board(spaceBuilder: SpaceBuilder())
      }
      
      context("default state") {
        it("defaults its marker to O") {
          expect(minimaxer.getMarker()).to(equal("O"))
        }
        
        it("marker can be specified") {
          let customMinimaxer = Minimaxer(marker: "bees",judge: judge)
          expect(customMinimaxer.getMarker()).to(equal("bees"))
        }
        
        it("defaults opponent marker to X") {
          expect(minimaxer.getOpponentMarker()).to(equal("X"))
        }
        
        it("marker can be specified") {
          let customMinimaxer = Minimaxer(opponentMarker: "bees", judge: judge)
          expect(customMinimaxer.getOpponentMarker()).to(equal("bees"))
        }
      }
      
      context("scoring a board") {
        it("scores boards where it loses correctly") {
          let xCoords = [Coords(x:0,y:0),Coords(x:0,y:1),Coords(x:0,y:2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents:"X")
          let gameState = GameState(board: board, activePlayer: playerX, inactivePlayer: playerO)
          
          let score = minimaxer.score(gameState)
          expect(score).to(equal(-10))
        }
        
        it("scores boards where it wins correctly") {
          let oCoords = [Coords(x:0,y:0),Coords(x:0,y:1),Coords(x:0,y:2)]
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents:"O")
          let gameState = GameState(board: board, activePlayer: playerX, inactivePlayer: playerO)
          
          let score = minimaxer.score(gameState)
          expect(score).to(equal(10))
        }
        
        it("scores boards where no one wins correctly") {
          let gameState = GameState(board: board, activePlayer: playerX, inactivePlayer: playerO)
          let score = minimaxer.score(gameState)
          expect(score).to(equal(0))
        }
      }
      
      context("one spot available") {
        let expectedChoice = Coords(x: 2, y: 2)
        
        it("chooses that position if it wins") {
          let oCoords = [Coords(x:0,y:0), Coords(x:0,y:2), Coords(x:1,y:1), Coords(x:1,y:2)]
          let xCoords = [Coords(x:0,y:1), Coords(x:1,y:0), Coords(x:2,y:0), Coords(x:2,y:1)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          let gameState = GameState(board: board, activePlayer: playerX, inactivePlayer: playerO)
          
          minimaxer.calculateMove(gameState)
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(expectedChoice))
          
        }
        
        it("chooses that position if it doesnt win") {
          let oCoords = [Coords(x:0,y:0), Coords(x:0,y:2), Coords(x:1,y:2), Coords(x:2,y:1)]
          let xCoords = [Coords(x:0,y:1), Coords(x:1,y:0),Coords(x:1,y:1), Coords(x:2,y:0)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          let gameState = GameState(board: board, activePlayer: playerX, inactivePlayer: playerO)
          
          minimaxer.calculateMove(gameState)
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(expectedChoice))
          
        }
      }
      
      context("two spots available, one win and one lose") {
        it("chooses the winning spot") {
          let oCoords = [Coords(x:0,y:0), Coords(x:1,y:1), Coords(x:1,y:2)]
          let xCoords = [Coords(x:0,y:1), Coords(x:1,y:0),Coords(x:2,y:0), Coords(x:0,y:2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 2, y: 2)))
          
        }
      }
      
      context("two spots available, one win and one block") {
        it("chooses the winning spot") {
          let oCoords = [Coords(x: 1, y: 1), Coords(x: 1, y: 2), Coords(x: 2, y: 0), Coords(x: 2, y: 1)]
          let xCoords = [Coords(x: 0, y: 1), Coords(x: 0, y: 2), Coords(x: 2, y: 2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 0, y: 0)))
        }
      }
      
      context("three spots available") {
        it("chooses winning move") {
          let oCoords = [Coords(x: 0, y: 2), Coords(x: 1, y: 1), Coords(x: 2, y: 1)]
          let xCoords = [Coords(x: 1, y: 2), Coords(x: 2, y: 0), Coords(x: 2, y: 2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 0, y: 1)))
        }
        
        it("blocks if no win available") {
          let oCoords = [Coords(x: 0, y: 0), Coords(x: 1, y: 2), Coords(x: 2, y: 0)]
          let xCoords = [Coords(x: 0, y: 1), Coords(x: 1, y: 0), Coords(x: 1, y: 1)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 2, y: 1)))
        }
      }
      
      context("four spots available") {
        it("chooses winning move") {
          let oCoords = [Coords(x: 0, y: 1), Coords(x: 1, y: 1)]
          let xCoords = [Coords(x: 1, y: 0), Coords(x: 1, y: 2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 2, y: 1)))
        }
        
        it("blocks if no win available") {
          let oCoords = [Coords(x: 0, y: 1), Coords(x: 1, y: 1)]
          let xCoords = [Coords(x: 1, y: 0), Coords(x: 1, y: 2)]
          setContentOfMultipleSpaces(board, coordsToSet: xCoords, newContents: "X")
          setContentOfMultipleSpaces(board, coordsToSet: oCoords, newContents: "O")
          
          minimaxer.calculateMove(GameState(board: board, activePlayer: playerX, inactivePlayer: playerO))
          let choice = minimaxer.getChoice()
          
          expect(choice).to(equal(Coords(x: 2, y: 1)))
        }
      }
      
      
    }
  }
}