class FakeMinimaxer: Minimaxer {
  var responseCoords = Coords(x: 0, y: 0)
  
  override func getMove(gameState: GameState) -> Coords {
    return responseCoords
  }
}
