class FakeJudge: Judge {
  var gameOverStatus = true
  var gameWonStatus = false
  var winner = ""
  
  override func isGameOver(gameState: GameState) -> Bool {
    return gameOverStatus
  }
  
  override func isGameWon(gameState: GameState) -> Bool {
    return gameWonStatus
  }
  
  override func getWinner(gameState: GameState) -> String {
    return winner
  }
  
  
}
