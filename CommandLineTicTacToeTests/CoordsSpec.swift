import Nimble
import Quick

class CoordsSpec: QuickSpec {
  override func spec() {
    describe("coord set") {
      
      context("two coord sets are equal") {
        it("reports they are equal, with symmetry") {
          let coordsA = Coords(x: 1, y: 2)
          let coordsB = Coords(x: 1, y: 2)
          expect(coordsA == coordsB).to(equal(true))
          expect(coordsB == coordsA).to(equal(true))
          expect(coordsA != coordsB).to(equal(false))
          expect(coordsB != coordsA).to(equal(false))
        }
      }
      
      context("two coord sets are unequal") {
        it("reports they are unequal, with symmetry") {
          let coordsC = Coords(x: 0, y: 2)
          let coordsD = Coords(x: 0, y: 3)
          expect(coordsC == coordsD).to(equal(false))
          expect(coordsD == coordsC).to(equal(false))
          expect(coordsC != coordsD).to(equal(true))
          expect(coordsD != coordsC).to(equal(true))
        }
      }
    }
  }
}