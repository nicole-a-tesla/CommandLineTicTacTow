
func setContentOfMultipleSpaces(board:Board, coordsToSet:[Coords], newContents:String) {
  for coords in coordsToSet {
    board.setSpaceContents(coords, newContents: newContents)
  }
}

func allContentsEqualThis(spaceCollection:[Space], expectedContents:String) -> Bool {
  let spacesThatMatch = spaceCollection.filter { $0.getContents() == expectedContents }
  return spacesThatMatch.count == spaceCollection.count
}

func generateTiedBoard() -> Board {
  let catsBoard = Board(spaceBuilder: SpaceBuilder())
  
  let xCoords = [Coords(x:0,y:0),Coords(x:1,y:1), Coords(x:1,y:2), Coords(x:2,y:0), Coords(x:2,y:1)]
  let oCoords = [Coords(x:0,y:1), Coords(x:0,y:2), Coords(x:1,y:0), Coords(x:2,y:2)]
  
  setContentOfMultipleSpaces(catsBoard, coordsToSet: xCoords, newContents: "X")
  setContentOfMultipleSpaces(catsBoard, coordsToSet: oCoords, newContents: "O")
  
  return catsBoard
}
