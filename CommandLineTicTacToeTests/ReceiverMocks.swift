class FakeReceiver: Receiver {
  var alwaysReturn: String? = "0"
  var getInputWasCalled = false
  
  override func getInput() -> String? {
    getInputWasCalled = true
    return alwaysReturn
  }
}