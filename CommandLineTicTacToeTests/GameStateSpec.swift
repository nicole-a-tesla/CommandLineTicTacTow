import Nimble
import Quick

class GameStateSpec: QuickSpec {
  override func spec() {
    
    describe("GameState") {
      var game:GameState!
      let playerX = Player(marker: "X", nextMoveDelegate: nil)
      let playerO = Player(marker: "O", nextMoveDelegate: nil)
      
      beforeEach {
        game = GameState(activePlayer: playerX, inactivePlayer: playerO)
      }
      
      context("default state") {
        it("has a isOver accessor that returns false") {
          expect(game.isOver()).to(equal(false))
        }
        
        it("has a hasWinner accessor that returns false") {
          expect(game.hasWinner()).to(equal(false))
        }
        
        it("handles requests for a nonexistant winner's (Null player's) marker gracefully") {
          expect(game.getWinningMarker()).to(equal("Null"))
        }
        
        it("has a default board") {
          expect(game.getBoard().getSpaceContents(Coords(x:0,y:0))).to(equal("Empty"))
        }
        
        it("has O as the default activePlayer") {
          expect(game.getActiveMarker()).to(equal("X"))
        }
        
        it("has X as the default inactivePlayer") {
          expect(game.getInactiveMarker()).to(equal("O"))
        }
      }
      
      context("modified state") {
        
        it("can update winner to a player, and updates winner and gameOver state accordingly") {
          let marker = playerX.getMarker()
          game = GameState(activePlayer: playerX, inactivePlayer: playerO)
          game.setWinner(marker)
          expect(game.hasWinner()).to(equal(true))
          expect(game.getWinningMarker()).to(equal(marker))
          expect(game.isOver()).to(equal(true))
        }
        
        it("can update gameover state to true independantly of win update") {
          game.setGameOverToTrue()
          expect(game.isOver()).to(equal(true))
        }
      }
      
      context("It can produce a next turn state") {
        let lastState = GameState(activePlayer: playerX, inactivePlayer: playerO)
        let spaceToModify = Coords(x: 0, y: 0)
        let nextState = lastState.getNextState(spaceToModify)
        
        it("sets the specified space to the value of the active player") {
          expect(nextState.getSpace(spaceToModify)).to(equal(Space(contents: lastState.getActiveMarker())))
        }
        
        it("switches the active and inactive players") {
          expect(nextState.getActiveMarker()).notTo(equal(lastState.getActiveMarker()))
          expect(nextState.getInactiveMarker()).notTo(equal(lastState.getInactiveMarker()))
        }
      }
    }
  }
}




