import Nimble
import Quick

class UserInputValidatorSpec: QuickSpec {
  override func spec() {
    
    describe("input validator") {
      let validator = UserInputValidator()
      var boardStub: FakeBoard!
      
      beforeEach {
        boardStub = FakeBoard(spaceBuilder: SpaceBuilder())
        boardStub.emptinessReturnValue = true
      }
      
      it("returns false for string that unwraps to nil") {
        let optionalNil: String? = nil
        expect(validator.isValid(optionalNil, board: boardStub)).to(equal(false))
      }
      
      it("returns true for string that unwraps to valid board position") {
        let validOptional: String? = "1a"
        expect(validator.isValid(validOptional, board: boardStub)).to(equal(true))
      }
      
      it("returns false for string that unwraps to garbage") {
        let validOptional: String? = "hats"
        expect(validator.isValid(validOptional, board: boardStub)).to(equal(false))
      }
      
      it("returns false for string that unwraps to a valid string with garbage on the end") {
        let validOptional: String? = "1ahats"
        expect(validator.isValid(validOptional, board: boardStub)).to(equal(false))
      }
      
      it("returns false for a reference to a taken spot") {
        let takenOptional: String? = "1a"
        boardStub.emptinessReturnValue = false
        expect(validator.isValid(takenOptional, board: boardStub)).to(equal(false))
      }
    }
    
  }
}