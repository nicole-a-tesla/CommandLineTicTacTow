class FakePrinter: ConsolePrinter {
  var argsToPrint = [String]()
  
  override func printIt(stuffToPrint: String) {
    argsToPrint.append(stuffToPrint)
  }
}