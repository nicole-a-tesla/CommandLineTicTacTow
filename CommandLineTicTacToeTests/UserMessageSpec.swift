import Nimble
import Quick

class UserMessageSpec: QuickSpec {
  override func spec() {
    
    describe("user messages") {
      let message = UserMessage()
      
      it("tells player its their turn") {
        expect(message.yourTurn("X")).to(equal("It's your turn, X!"))
      }
      
      it("tells player they've failed") {
        expect(message.fail()).to(equal("Seems like you're having a hard time. Maybe try again later."))
      }
      
      it("tells player to pick a valid spot") {
        expect(message.choiceInvalid()).to(equal("That's not a valid position. Pick a valid position"))
      }
      
      it("tells player the game is over") {
        expect(message.gameOver()).to(equal("Game Over"))
      }
      
      it("announces a winner") {
        expect(message.announceWinner("O")).to(equal("O Wins"))
      }
      
      it("announces a tie") {
        expect(message.announceTie()).to(equal("It's a tie"))
      }
      
      it("prompts move") {
        expect(message.promptMove()).to((equal("Enter a number+letter combination i.e: 2b")))
      }
    }
  }
}