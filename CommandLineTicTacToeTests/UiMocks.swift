class FakeConsoleUI: ConsoleUi {
  var responseCoords = Coords(x: 0, y: 0)
  var handleGameEndCalled = false
  var announceNewTurnCalled = false
  
  override func requestPositionUntilValid(board: Board) -> Coords {
    return responseCoords
  }
  
  override func handleGameEnd(gameState: GameState) {
    handleGameEndCalled = true
  }
  
  override func announceNewTurn(gameState: GameState) {
    announceNewTurnCalled = true
  }
}

