class FakeBoard: Board {
  var emptinessReturnValue = true
  
  override func isSpaceEmpty(coords: Coords) -> Bool {
    return emptinessReturnValue
  }
}