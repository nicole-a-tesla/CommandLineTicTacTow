import Nimble
import Quick

class NullPlayerSpec: QuickSpec {
  override func spec() {
    describe("Null Player") {
      it("marker defaults to Null") {
        let nullPlayer = NullPlayer()
        expect(nullPlayer.getMarker()).to(equal("Null"))
      }
      
      it("ignores attempts to init with different marker") {
        let nullPlayer = NullPlayer(marker: "dog face")
        expect(nullPlayer.getMarker()).to(equal("Null"))
      }
    }
  }
}