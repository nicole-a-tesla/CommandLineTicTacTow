import Nimble
import Quick

class SpaceSpec: QuickSpec {
  override func spec() {
    describe("a space") {
      it("has contents that initialize empty") {
        let space = Space()
        expect(space.getContents()).to(equal("Empty"))
      }
      
      it("reports on its emptiness") {
        let space = Space()
        expect(space.isEmpty()).to(equal(true))
      }
      
      it("reports on its lack of emptiness") {
        let space = Space(contents: "X")
        expect(space.isEmpty()).to(equal(false))
      }
      
      it("reports equality correctly"){
        let spaceA = Space(contents: "X")
        let spaceB = Space(contents: "X")
        expect(spaceA == spaceB).to(equal(true))
        expect(spaceB == spaceA).to(equal(true))
        expect(spaceA != spaceB).to(equal(false))
        expect(spaceB != spaceA).to(equal(false))
      }
      
      it("reports inequality correctly") {
        let spaceC = Space(contents: "O")
        let spaceD = Space(contents: "X")
        expect(spaceC == spaceD).to(equal(false))
        expect(spaceD == spaceC).to(equal(false))
        expect(spaceC != spaceD).to(equal(true))
        expect(spaceD != spaceC).to(equal(true))
      }
      
    }
  }
}