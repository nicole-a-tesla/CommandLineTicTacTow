class FakeGameState: GameState {
  var gameOver = false
  var winner: Player = NullPlayer(nextMoveDelegate: nil)
  
  override func hasWinner() -> Bool {
    return winner.getMarker() != "Null"
  }
  
}