class FakeValidator: UserInputValidator {
  var lastIsValidArg: String?
  
  override func isValid(input: String?, board: Board) -> Bool {
    lastIsValidArg = input
    return true
  }
  
  func receivedIsValidWithArgs(arg: String) -> Bool {
    return lastIsValidArg == arg
  }
}