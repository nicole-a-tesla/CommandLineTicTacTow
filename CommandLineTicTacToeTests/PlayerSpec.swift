import Nimble
import Quick

class PlayerSpec: QuickSpec {
  override func spec() {
    describe("player") {
      it("has an accessible marker") {
        let player = Player(marker: "dog face", nextMoveDelegate: nil)
        expect(player.getMarker()).to(equal("dog face"))
      }
      
      context("delegated move getting") {
        let userMessage = UserMessage()
        let receiver = ConsoleReceiver()
        let formatter = BoardPrintFormatter(parser: BoardParser())
        let validator = UserInputValidator()
        let inputMap = PositionCoordsMap()
        let printer = ConsolePrinter()
        
        let ui = FakeConsoleUI(printer: printer, printFormatter: formatter, receiver: receiver, validator: validator, positionCoordsMap: inputMap, userMessage: userMessage)
        
        let strategy = FakeMinimaxer()
        let playerX = Player(marker: "X", nextMoveDelegate: ui)
        let playerO = Player(marker: "O", nextMoveDelegate: strategy)
        let gameState = GameState(activePlayer: playerX, inactivePlayer: playerO)
        
        it("gets coords from ui") {
          let recievedCoords = playerX.getMove(gameState)
          expect(recievedCoords).to(equal(ui.responseCoords))
        }
        
        it("gets coords from strategy") {
          let move = playerO.getMove(gameState)
          expect(move).to(equal(strategy.responseCoords))
        }
      }
      
    }
  }
}