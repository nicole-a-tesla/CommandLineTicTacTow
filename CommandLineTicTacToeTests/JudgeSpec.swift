import Nimble
import Quick

class JudgeSpec: QuickSpec {
  override func spec() {
    
    describe("a judge") {
      let playerX = Player(marker: "X", nextMoveDelegate: nil)
      let playerO = Player(marker: "O", nextMoveDelegate: nil)
      let judge = Judge()
      
      context("empty board") {
        let gameState = GameState(activePlayer: playerX, inactivePlayer: playerO)
        
        it(".isTie returns false for empty game") {
          expect(judge.isTie(gameState)).to(equal(false))
        }
        
        it(".isGameWon returns false for empty game") {
          expect(judge.isGameWon(gameState)).to(equal(false))
        }
        
        it(".getWinner returns no marker") {
          expect(judge.getWinner(gameState)).to(equal("Null"))
        }
        
        it(".isGameOver reports incomplete game for empty board") {
          expect(judge.isGameOver(gameState)).to(equal(false))
        }
      }
      
      context("game is tied") {
        let gameState = GameState(board: generateTiedBoard(), activePlayer: playerX, inactivePlayer: playerO)
        
        it(".isTie returns true for tied game") {
          expect(judge.isTie(gameState)).to(equal(true))
        }
        
        it(".isGameWon returns false for tied game") {
          expect(judge.isGameWon(gameState)).to(equal(false))
        }
        
        it(".getWinner returns no marker") {
          expect(judge.getWinner(gameState)).to(equal("Null"))
        }
        
        it(".isGameOver reports complete game in event of tie") {
          expect(judge.isGameOver(gameState)).to(equal(true))
        }
      }
      
      context("game is won - non-full board") {
        let wonBoard = Board(spaceBuilder: SpaceBuilder())
        let topRow = [Coords(x: 0, y: 0), Coords(x: 0, y: 1), Coords(x: 0, y: 2)]
        setContentOfMultipleSpaces(wonBoard, coordsToSet: topRow, newContents: "X")
        let gameState = GameState(board: wonBoard, activePlayer: playerX, inactivePlayer: playerO)
        
        it(".isTie returns false") {
          expect(judge.isTie(gameState)).to(equal(false))
        }
        
        it(".isGameOver reports complete game for won board") {
          expect(judge.isGameOver(gameState)).to(equal(true))
        }
        
        it(".getWinner returns winning marker") {
          expect(judge.getWinner(gameState)).to(equal("X"))
        }
        
        it(".isGameWon returns true for won board") {
          expect(judge.isGameWon(gameState)).to(equal(true))
        }
      }
      
      context("game is won - full board") {
        let fullWin = Board(spaceBuilder: SpaceBuilder())
        let xCoords = [Coords(x: 0, y: 0), Coords(x: 0, y: 2), Coords(x: 0, y: 1), Coords(x: 1, y: 2),  Coords(x: 2, y: 0)]
        let oCoords = [Coords(x: 1, y: 0), Coords(x: 1, y: 1), Coords(x: 2, y: 1), Coords(x: 2, y: 2)]
        setContentOfMultipleSpaces(fullWin, coordsToSet: xCoords, newContents: "X")
        setContentOfMultipleSpaces(fullWin, coordsToSet: oCoords, newContents: "O")
        let gameState = GameState(board: fullWin, activePlayer: playerX, inactivePlayer: playerO)
        
        it(".isTie returns false") {
          expect(judge.isTie(gameState)).to(equal(false))
        }
        
        it(".isGameWon returns true") {
          expect(judge.isGameWon(gameState)).to(equal(true))
        }
        
        it(".getWinner returns winning marker") {
          expect(judge.getWinner(gameState)).to(equal("X"))
        }
        
        it(".isGameOver returns true") {
          expect(judge.isGameOver(gameState)).to(equal(true))
        }
      }
      
      context("in progress game, no winner") {
        let partialBoard = Board(spaceBuilder: SpaceBuilder())
        let randomCoords = [Coords(x: 0, y: 0), Coords(x: 1, y: 1), Coords(x: 0, y: 2)]
        setContentOfMultipleSpaces(partialBoard, coordsToSet: randomCoords, newContents: "X")
        let gameState = GameState(board: partialBoard, activePlayer: playerX, inactivePlayer: playerO)
        
        it(".isTie returns false") {
          expect(judge.isTie(gameState)).to(equal(false))
        }
        
        it(".isGameWon returns false") {
          expect(judge.isGameWon(gameState)).to(equal(false))
          
        }
        
        it(".getWinner returns no marker") {
          expect(judge.getWinner(gameState)).to(equal("Null"))
        }
        
        
        it(".isGameOver reports incomplete game for partially complete board") {
          expect(judge.isGameOver(gameState)).to(equal(false))
        }
      }
      
      context("internal functions") {
        it("reports subBoard win") {
          let winningRow = [Space(contents: "X"),Space(contents: "X"),Space(contents: "X")]
          expect(judge.checkForSubBoardWin(winningRow)).to(equal(true))
        }
        
        it("doesn't report a win for non-winning subBoard") {
          let nonWinningRow = [Space(contents: "X"),Space(contents: "X"),Space(contents: "O")]
          expect(judge.checkForSubBoardWin(nonWinningRow)).to(equal(false))
        }
      }
      
      it("confirms identical item list") {
        expect(judge.allElementsIdentical([Space(contents: "x"),Space(contents: "x"),Space(contents: "x")])).to(equal(true))
      }
      
      it("reports list of different items not identical") {
        expect(judge.allElementsIdentical([Space(contents: "x"),Space(contents: "Empty"),Space(contents: "x")])).to(equal(false))
      }
      
    }
  }
}