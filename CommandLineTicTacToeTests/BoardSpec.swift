import Nimble
import Quick

class BoardSpec: QuickSpec {
  override func spec() {
    
    describe("the board") {
      var board: Board!
      let coords = Coords(x:0, y:0)
      
      beforeEach {
        board = Board(spaceBuilder: SpaceBuilder())
      }
      
      it("can set spaces to values") {
        board.setSpaceContents(coords, newContents:"X")
        expect(board.isSpaceEmpty(coords)).to(equal(false))
      }
      
      it("returns space's contents") {
        expect(board.getSpaceContents(coords)).to(equal("Empty"))
      }
      
      it("returns a space") {
        let space = board.getSpaces()[0][0]
        expect(board.getSpace(Coords(x: 0, y: 0))).to(equal(space))
      }
      
      it("can return a clone itself") {
        let coord0 = Coords(x:0, y:0)
        let coord1 = Coords(x:1, y:1)
        board.setSpaceContents(coord1, newContents: "X")
        let boardClone = board.clone()
        
        expect(boardClone).toNot(be(board))
        
        expect(boardClone.getSpace(coord0)).to(equal(board.getSpace(coord0)))
        expect(boardClone.getSpace(coord0)).notTo(be(board.getSpace(coord0)))
        
        expect(boardClone.getSpace(coord1)).to(equal(board.getSpace(coord1)))
        expect(boardClone.getSpace(coord1)).notTo(be(board.getSpace(coord1)))
        
        
      }
    }
  }
}