class FakePlayer: Player {
  var askedForMove = false
  let responseCoords = Coords(x: 0, y: 0)
  
  override init(marker: String, nextMoveDelegate: NextMoveDelegate?=nil) {
    super.init(marker: marker, nextMoveDelegate: nextMoveDelegate)
  }
  
  override func getMove(gameState: GameState) -> Coords {
    askedForMove = true
    return responseCoords
  }
}