import Nimble
import Quick

class GameControllerSpec: QuickSpec {
  override func spec() {
    
    describe("Game Controller") {
      var humanSpy: FakePlayer!
      var computerSpy: FakePlayer!
      var mockUi: FakeConsoleUI!
      var board: Board!
      
      let judge = Judge()
      
      let userMessage = UserMessage()
      let receiver = ConsoleReceiver()
      let formatter = BoardPrintFormatter(parser: BoardParser())
      let validator = UserInputValidator()
      let inputMap = PositionCoordsMap()
      let printer = ConsolePrinter()
      
      beforeEach {
        humanSpy = FakePlayer(marker: "X")
        computerSpy = FakePlayer(marker: "O")
        mockUi = FakeConsoleUI(printer: printer, printFormatter: formatter, receiver: receiver, validator: validator, positionCoordsMap: inputMap, userMessage: userMessage)
        board = Board(spaceBuilder: SpaceBuilder())
      }
      
      context("taking turns") {
        
        it("on taketurn(), activePlayer is asked for a move") {
          let state = GameState(board: board, activePlayer: humanSpy, inactivePlayer: computerSpy)
          let controller = GameController(gameState: state, judge: judge, ui: mockUi)
          
          controller.takeTurn()
          
          expect(humanSpy.askedForMove).to(equal(true))
          expect(computerSpy.askedForMove).to(equal(false))
        }
        
        it("generates a new gameState reflective of activePlayer's choice") {
          let initialState = GameState(board: board, activePlayer: computerSpy, inactivePlayer: humanSpy)
          let controller = GameController(gameState: initialState, judge: judge, ui: mockUi)
          
          controller.takeTurn()
          
          let postTurnState = controller.getGameState()
          expect(initialState).notTo(be(postTurnState))
          
          let expectedSpace = Space(contents: computerSpy.getMarker())
          let actualSpace = postTurnState.getSpace(computerSpy.responseCoords)
          expect(expectedSpace).to(equal(actualSpace))
        }
        
        it("tells the ui to announce new turn") {
          let state = GameState(board: board, activePlayer: computerSpy, inactivePlayer: humanSpy)
          let controller = GameController(gameState: state, judge: judge, ui: mockUi)
          
          controller.takeTurn()
          expect(mockUi.announceNewTurnCalled).to(equal(true))
        }
      }
      
      context("when the game is over") {
        let gameAlwaysOverJudge = FakeJudge()
        var initialState: GameState!
        var controller: GameController!
        
        beforeEach {
          initialState = GameState(board: board, activePlayer: humanSpy, inactivePlayer: computerSpy)
          controller = GameController(gameState: initialState, judge: gameAlwaysOverJudge, ui: mockUi)
          
        }
        
        it("turn taking stops when game is over") {
          controller.start()
          let postTurnState = controller.getGameState()
          
          expect(postTurnState).to(be(initialState))
        }
        
        it("sends the Ui a gameOverMessage when game is over") {
          controller.start()
          expect(mockUi.handleGameEndCalled).to(equal(true))
        }
        
        it("updates the gameState.gameOver to true") {
          controller.start()
          let endState = controller.getGameState()
          expect(endState.isOver()).to(equal(true))
        }
        
        it("updates the gameState.winner if there is one") {
          gameAlwaysOverJudge.gameWonStatus = true
          gameAlwaysOverJudge.winner = "O"
          
          controller.start()
          let endState = controller.getGameState()
          expect(endState.hasWinner()).to(equal(true))
        }
        
        
      }
    }
  }
}
