import Nimble
import Quick

class SpaceBuilderSpec: QuickSpec {
  override func spec() {
    
    describe("space builder") {
      let builder = SpaceBuilder()
      let built = builder.build()
      
      it("builds 3 rows") {
        expect(built.count).to(equal(3))
      }
      
      it("builds 9 spaces") {
        expect(built.flatMap{ $0 }.count).to(equal(9))
      }
      
      it("spaces initialize as empty") {
        expect(built[0][0].isEmpty()).to(equal(true))
      }
      
    }
  }
}